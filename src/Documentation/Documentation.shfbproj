﻿<?xml version="1.0" encoding="utf-8"?>
<Project DefaultTargets="Build" xmlns="http://schemas.microsoft.com/developer/msbuild/2003" ToolsVersion="4.0">
  <PropertyGroup>
    <!-- The configuration and platform will be used to determine which
         assemblies to include from solution and project documentation
         sources -->
    <Configuration Condition=" '$(Configuration)' == '' ">Debug</Configuration>
    <Platform Condition=" '$(Platform)' == '' ">AnyCPU</Platform>
    <SchemaVersion>2.0</SchemaVersion>
    <ProjectGuid>{1640949a-69ac-4fae-aa36-0554e7340e24}</ProjectGuid>
    <SHFBSchemaVersion>1.9.5.0</SHFBSchemaVersion>
    <!-- AssemblyName, Name, and RootNamespace are not used by SHFB but Visual
         Studio adds them anyway -->
    <AssemblyName>Documentation</AssemblyName>
    <RootNamespace>Documentation</RootNamespace>
    <Name>Documentation</Name>
    <!-- SHFB properties -->
    <OutputPath>.\Help\</OutputPath>
    <HtmlHelpName>DotNetAuth Documentation</HtmlHelpName>
    <Language>en-US</Language>
    <DocumentationSources>
      <DocumentationSource sourceFile="..\DotNetAuth\DotNetAuth.csproj" />
<DocumentationSource sourceFile="..\DotNetAuth.Profiles\DotnetAuth.Profiles.csproj" /></DocumentationSources>
    <BuildAssemblerVerbosity>OnlyWarningsAndErrors</BuildAssemblerVerbosity>
    <HelpFileFormat>HtmlHelp1, MSHelpViewer, Website</HelpFileFormat>
    <IndentHtml>False</IndentHtml>
    <FrameworkVersion>.NET Framework 4.0</FrameworkVersion>
    <KeepLogFile>True</KeepLogFile>
    <DisableCodeBlockComponent>False</DisableCodeBlockComponent>
    <CppCommentsFixup>False</CppCommentsFixup>
    <CleanIntermediates>True</CleanIntermediates>
    <SyntaxFilters>CSharp, VisualBasic</SyntaxFilters>
    <SdkLinkTarget>Blank</SdkLinkTarget>
    <RootNamespaceTitle>API Documentation</RootNamespaceTitle>
    <RootNamespaceContainer>True</RootNamespaceContainer>
    <PresentationStyle>vs2010</PresentationStyle>
    <Preliminary>True</Preliminary>
    <NamingMethod>MemberName</NamingMethod>
    <HelpTitle>A .Net OAuth Library</HelpTitle>
    <FeedbackEMailLinkText>Feedback</FeedbackEMailLinkText>
    <FeedbackEMailAddress>sam.n%40live.com.au</FeedbackEMailAddress>
    <ContentPlacement>AboveNamespaces</ContentPlacement>
    <NamespaceSummaries>
      <NamespaceSummaryItem name="DotNetAuth.Diagnostics" isDocumented="True">Logging infrastructure and other diagnostics features.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth1a" isDocumented="True">Types to implement OAuth 1.0 a protocol.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth1a.Framework" isDocumented="True">More abstract types used to support an OAuth 1.0a workflow.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth1a.Providers" isDocumented="True">The default implementations for OAuth1.0a providers such as Yahoo and Twitter.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth2" isDocumented="True">Types to implement OAuth 2 a protocol.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth2.Framework" isDocumented="True">More abstract types used to support an OAuth 2 workflow.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.OAuth2.Providers" isDocumented="True">The default implementations for OAuth1.0a providers such as Google and Microsoft Account.</NamespaceSummaryItem>
<NamespaceSummaryItem name="DotNetAuth.Profiles" isDocumented="True">Types which allows you to use OAuth protocol as a user and profile management component.</NamespaceSummaryItem></NamespaceSummaries>
    <ProjectSummary>DotNetAuth library is a simple library that aims to help you implement OAuth 1.0a or OAuth 2 protocol to access to online APIs. This library only support server-side flow. So if you are a web application developer who wants to authenticate users using OAuth or you want to be granted access to some online API such as Google Plus API, then this library is for you.</ProjectSummary>
    <VisibleItems>InheritedMembers, Protected, SealedProtected</VisibleItems>
    <MissingTags>Summary, Parameter, Returns, Value, Remarks, AutoDocumentCtors, Namespace, TypeParameter, AutoDocumentDispose</MissingTags>
    <ComponentConfigurations />
    <PlugInConfigurations>
      <PlugInConfig id="Hierarchical Table of Contents" enabled="True">
        <configuration>
          <toc minParts="1" insertBelow="True" />
        </configuration>
      </PlugInConfig>
    </PlugInConfigurations>
    <ApiFilter />
    <HelpAttributes />
    <BuildLogFile />
    <HtmlHelp1xCompilerPath />
    <HtmlHelp2xCompilerPath />
    <SandcastlePath />
    <WorkingPath />
  </PropertyGroup>
  <!-- There are no properties for these groups.  AnyCPU needs to appear in
       order for Visual Studio to perform the build.  The others are optional
       common platform types that may appear. -->
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|AnyCPU' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x86' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|x64' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Debug|Win32' ">
  </PropertyGroup>
  <PropertyGroup Condition=" '$(Configuration)|$(Platform)' == 'Release|Win32' ">
  </PropertyGroup>
  <ItemGroup>
    <ContentLayout Include="Content Layout.content" />
  </ItemGroup>
  <ItemGroup>
    <None Include="About OAuth.aml" />
    <None Include="Adding or Modifying Providers.aml" />
    <None Include="Authentication using OAuth 2.0.aml" />
    <None Include="Authentication vs Authorization.aml" />
    <None Include="Authenticatoin using OAuth 1.aml" />
    <None Include="DotNetAuth Features.aml" />
    <None Include="Introduction.aml" />
    <None Include="Registering your application.aml" />
    <None Include="Setting up test environment.aml" />
    <None Include="User Profiles.aml" />
    <None Include="Using DotNetAuth.aml" />
  </ItemGroup>
  <!-- Import the SHFB build targets -->
  <Import Project="$(SHFBROOT)\SandcastleHelpFileBuilder.targets" />
</Project>