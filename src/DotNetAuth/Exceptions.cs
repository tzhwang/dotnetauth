﻿using System;

namespace DotNetAuth
{
    /// <summary>
    /// This exception indicates there was a problem in retrieving state of a an authentication.
    /// </summary>
    /// <remarks>
    /// <para>
    /// State management helps the oauth to keep integrity of authentication processes. When you redirect users to a third party website(e.g. Facebook) and then 
    /// the third party redirects back them to your website. You need to know which user is back and associate the different requests to your website to a single
    /// process of authentication. So it is important to check if the state is present and is valid and is not tampered.
    /// </para>
    /// <para>
    /// In OAuth 2.0 you must make sure that you do not call <see cref="OAuth2.OAuth2Process.ProcessUserResponse"/> twice for the same authentication process.
    /// As a piece of advice, Imagine if you have ProcessUserResponse.aspx page which Facebook redirects back users to it, then you make a call to <see cref="OAuth2.OAuth2Process.ProcessUserResponse"/>
    /// and receive the outcome(a successful authentication or being rejected etc). Now that you have the outcome then you may keep the users in the same
    /// ProcessUserResponse.aspx page or you may redirect them to a different page. 
    /// If you keep users in the same page (for example showing a message 'Congratulation, you just signed up!') then there are chances that user refresh the page,
    /// hence a new GET request. And then this exception will occur. Also if user navigates to another page and returns back ProcessUserResponse.aspx via an 
    /// old browser which does not honour cache control attributes then a new GET request will be issued and this exception will arise.
    /// </para>
    /// </remarks>
    public class InvalidOAuthStateException : Exception
    {
        public InvalidOAuthStateException()
            : base("Invalid state or state not recognized.")
        {
        }
    }
}
