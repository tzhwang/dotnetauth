﻿using System;

namespace DotNetAuth.OAuth2
{
    /// <summary>
    /// Adds extension methods for <see cref="RestSharp.Http"/> and <see cref="System.Uri"/> in regard to OAuth 2.0 needs.
    /// </summary>
    public static class OAuthAuthorizedCalls
    {
        /// <summary>
        /// Adds the specified access token to the URI as a query string.
        /// </summary>
        /// <param name="uri">The URI to be modified.</param>
        /// <param name="accessToken">The access token to be added to URI.</param>
        /// <returns>A new Uri object which has accessToken as part of its query string.</returns>
        public static Uri AddAccessTokenToQueryString(this Uri uri, string accessToken)
        {
            var uribuilder = new UriBuilder(uri);
            uribuilder.Query = (uribuilder.Query.Length > 0 ? uribuilder.Query + "&" : string.Empty) + "access_token=" + accessToken;
            return uribuilder.Uri;
        }
        /// <summary>
        /// Adds the accessToken to current <see cref="RestSharp.Http"/> object as a header with name Authorization.
        /// </summary>
        /// <param name="http">The http object to be modified.</param>
        /// <param name="accessToken">The access token value to be added.</param>
        public static void ApplyAccessTokenToHeader(this RestSharp.Http http, string accessToken)
        {
            http.Headers.Add(new RestSharp.HttpHeader { Name = "Authorization", Value = "Bearer " + accessToken });
        }
    }
}
