﻿using System.Linq;
using DotNetAuth.OAuth2.Framework;
using RestSharp.Contrib;

namespace DotNetAuth.OAuth2.Providers
{
    /// <summary>
    /// OAuth 2 provider for github.
    /// </summary>
    public class GithubOAuth2 : OAuth2ProviderDefinition
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="GithubOAuth2"/> class.
        /// </summary>
        public GithubOAuth2()
        {
            AuthorizationEndpointUri = "https://github.com/login/oauth/authorize";
            TokenEndpointUri = "https://github.com/login/oauth/access_token";
        }
        /// <summary>
        /// Parses the response body to request to access token.
        /// </summary>
        /// <param name="body">The response body of request for access token.</param>
        /// <returns>A <see cref="ProcessUserResponseOutput"/> object containing access token and some additional parameters or error details.</returns>
        public override ProcessUserResponseOutput ParseAccessTokenResult(string body)
        {
            var values = HttpUtility.ParseQueryString(body);
            var allParameters = new ParameterSet();
            foreach (var v in values.Keys.Cast<string>()) allParameters.Add(v, values[v]);
            var accessToken = values["access_token"];
            return new ProcessUserResponseOutput(allParameters, accessToken);
        }
    }
}
