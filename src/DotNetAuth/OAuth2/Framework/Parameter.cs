namespace DotNetAuth.OAuth2.Framework
{
    /// <summary>
    /// Encapsulates a name and value.
    /// </summary>
    public class Parameter
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Parameter"/> class.
        /// </summary>
        public Parameter() { }
        /// <summary>
        /// Initializes a new instance of the <see cref="Parameter"/> class.
        /// </summary>
        /// <param name="name">The parameter's name.</param>
        /// <param name="value">The parameter's value.</param>
        public Parameter(string name, string value)
        {
            Name = name;
            Value = value;
        }

        /// <summary>
        /// Gets or sets the name of parameter.
        /// </summary>
        /// <value>
        /// The name of parameter.
        /// </value>
        public string Name { get; set; }
        /// <summary>
        /// The value for the parameter.
        /// </summary>
        /// <remarks>
        /// This value will be used in URI or any other part of communication. If some sort of encoding is required(like URL encoding), it should be performed beforehand.
        /// </remarks>
        /// <value>
        /// The value of parameter..
        /// </value>
        public string Value { get; set; }
    }
}
