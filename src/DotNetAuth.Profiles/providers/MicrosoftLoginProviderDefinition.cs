using Newtonsoft.Json.Linq;
namespace DotNetAuth.Profiles
{
    public class MicrosoftLoginProviderDefinition : LoginProviderDefinition
    {
        ProfileProperty[] supportedProperties;

        public MicrosoftLoginProviderDefinition()
        {
            Name = "Microsoft";
            Fullname = "Microsoft Account";
            ProtocolType = ProtocolTypes.OAuth2;
        }
        public override ProfileProperty[] GetSupportedProperties()
        {
            supportedProperties = new[] {
             ProfileProperty.UniqueID,
             ProfileProperty.DisplayName,
             ProfileProperty.FirstName,
             ProfileProperty.LastName,
             ProfileProperty.ProfileLink,
             ProfileProperty.BirthDate,
             ProfileProperty.Gender,
             ProfileProperty.Email,
             ProfileProperty.Locale,             
            };
            return supportedProperties;
        }
        public override string GetProfileEndpoint(ProfileProperty[] requiredProperties)
        {
            return "https://apis.live.net/v5.0/me/";
        }
        public override string GetRequiredScope(ProfileProperty[] requiredProperties)
        {
            //http://msdn.microsoft.com/en-us/library/live/hh243648.aspx#user
            return "wl.basic,wl.birthday,wl.emails,wl.postal_addresses";
        }
        public override Profile ParseProfile(string content)
        {
            var json = JObject.Parse(content);
            var year = json.Value<string>("birth_year");
            var month = json.Value<string>("birth_month");
            var day = json.Value<string>("birth_day");

            var result = new Profile
                {
                    UniqueID = json.Value<string>("id"),
                    FullName = json.Value<string>("name"),
                    FirstName = json.Value<string>("first_name"),
                    LastName = json.Value<string>("last_name"),
                    ProfileLink = json.Value<string>("link"),
                    Gender = json.Value<string>("gender"),
                    Email = json.Value<JObject>("emails").Value<string>("account"),
                    Locale = json.Value<string>("locale"),
                    BirthDate = year != null && month != null && day != null ? year + "/" + month + "/" + day : null
                };

            return result;
        }
        public override OAuth2.OAuth2ProviderDefinition GetOAuth2Definition()
        {
            return new OAuth2.Providers.LiveOAuth2();
        }
    }
}
