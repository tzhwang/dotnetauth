﻿using System;
using System.Reflection;

[assembly: AssemblyTitle("DotNetAuth.Profiles")]
[assembly: AssemblyDescription("Let's users login using their social network credentials and provides you with a user profile.")]
[assembly: AssemblyProduct("DotNetAuth.Profiles")]
[assembly: AssemblyCopyright("Copyright © Sam Naseri 2012")]
[assembly: AssemblyVersion("1.0.0.0")]
[assembly: AssemblyFileVersion("1.0.0.0")]
[assembly: CLSCompliant(true)]